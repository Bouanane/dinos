﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointScript : MonoBehaviour
{
    public float points;
    public float increase;
    public Text Score;
    // Start is called before the first frame update
    void Start()
    {
        points = 0f;
        increase = 30.0f;
    }

    // Update is called once per frame
    void Update()
    {
        Score.text =(int ) points + "";
        points += increase * Time.deltaTime;
    }
}
